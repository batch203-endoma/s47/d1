console.log("Hello World");


// [SECTION] Document Object Model
// Allow us to be able to access or modify the properties of an html element in a webpage.
//it is a standard on how to get, change, add or delete html elements.

// we will focus on using dom for managing forms.

// for selecting HTML elements will be using the documents.querySelector

// Syntax : document.querySelector("htmlElement")
// document referes to the whole page.
//."querySelector" is used to select a specific object (HTML elements) from the document (webpage)
// the query selector function takes a string input that is formatted like a CSS selector when applying the styles.


// aternatively, we can use the getElement functions to retrieve the elements.
// document.getElementById("txt-first-name");

// however, using these functions require us to indentify beforehand how we get the elements. with querySelector, we can be flexible in how to retireve the elements.


const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// [SECTION] Event Listeners
// whenever a user interacts with a web page, this action is considered as an event.
// working with events is large part of creating interactivity in a webpage.
//To perform an action when an event triggers, you first need to listen to it.

//The function use is "addEventListener" that takes two arguments:
// A string identifying an event;
// and a function that the listener will execute once the "specified event" is triggered.
//when an event occurs, an "event object" is passed to thefunction arugment as the first parameter.
txtFirstName.addEventListener("keyup", (event) => {
  console.log(event.target); //contains the element
  console.log(event.target.value); //contains the actual value.
});

txtFirstName.addEventListener("keyup", (e) => {
  //The "innerHTML" property sets or returns the HTML conent(innerHTML) of an element(div, span, etc.)
  spanFullName.innerHTML = `${txtFirstName.value}`;
})

//creating Multiple events that will trigger a same function

const fullName = () => {
  spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
}

txtFirstName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);

//Activity s47
/*
1. Create an addEventListener that will "change the color" of the "spanFullName".
2. Add a "select tag" element with the options/values of red, green, and blue in your index.html.
3. The values of the select tag will be use to change the color of the span with the id "spanFullName"

Check the following links to solve this activity:
    HTML DOM Events: https://www.w3schools.com/jsref/dom_obj_event.asp
    HTML DOM Style Object: https://www.w3schools.com/jsref/dom_obj_style.asp

*/

const textColor = document.querySelector("#text-color");

// let colorSelected = textColor.value;

textColor.addEventListener("change", () => {
  spanFullName.style.color = textColor.value.toLowerCase();
})









////
